# This code will relate the output from the RGA to possible componets solving Ax=b problem
#
# 2019-06-13 W. D. Lee -- originated
# 2019-06-27 W. D. Lee -- cleaned up the plots
# 2019-07-02 W. D. Lee -- fixed the matrix multiplication to be correct, imporved the commenting

import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime
from tango import DeviceProxy
from drawnow import drawnow, figure
import time
from scipy.signal import find_peaks

### this function builds the matrix, inverts it and does the multiplication, out putting a 
### vector of length equal to the number of "test gases" 

wavelengthtable = [[-100,356.052455,357.763636,358.946441,361.8547,365.769,365.829,365.908,365.969,366.045,366.136,366.231,366.326,366.445,366.569,366.712,366.877,367.05,367.237,367.486,367.7423,368.0418,368.3871,368.7881,369.2602,369.8209,370.4913,371.3034,372.3005,373.5431,375.1217,377.1704,379.8987,383.6485,386.9083,389.0166,392.009,392.18,397.1198,399.613,400.98839,407.31541,407.5508,410.2892,411.39756,413.288864,422.91329,427.873161,429.5814,433.241703,434.1692,434.928576,437.198114,437.26098,438.089709,440.22222,442.72436,443.14324,444.828,448.306791,454.632581,458.063257,459.118366,460.277,461.085816,463.184,464.438,464.872,465.155,465.920495,466.4361,472.819041,473.72302,476.619677,480.736348,484.916386,486.271,488.122627,491.110431,493.340225,496.64649,500.288,500.655,500.872,501.856194,504.651,505.355783,514.659,538.18269,559.4855,566.82,567.76,568.114,579.4722,580.2202,589.14,593.342,594.33,611.661581,648.384,656.46,657.987,658.47,658.9428,661.239,696.7352,706.9167,710.2081,711.0895,711.3436,711.5141,711.715,711.715,711.8952,712.1634,712.4159,713.4078,714.9012,723.331,723.841,727.494,732.8025,738.6014,740.19219,742.568,744.434,747.037,750.5935,751.6721,756.40961,758.111,758.1937,763.7208,765.4439,766.455,768.729,769.4612,772.5887,772.6333,772.8763,777.408,777.631,777.753,782.2729,782.7806,783.4784,783.9261,784.2427,785.0405,785.5023,786.304,789.2739,789.79771,790.47221,794.66221,795.0362,797.4224,799.8992,801.699,804.7546,806.081,806.4573,807.2635,808.0701,808.602,809.77528,810.5921,811.7542,821.86,823.7536,826.6794,827.98278,833.74351,839.471,841.0521,841.563,842.6963,844.027,844.106,844.857,844.868,844.908,846.959],
[3,40,40,40,184,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,184,1,12,12,1,14,184,40,184,1,40,40,64,40,184,40,1,40,40,12,40,40,40,40,14,40,40,40,40,14,40,14,14,12,12,40,27,40,40,40,40,40,1,40,64,12,40,14,14,14,40,14,12,12,12,27,14,14,14,12,12,12,14,14,40,14,1,12,12,12,14,40,40,12,12,12,12,12,12,12,12,12,12,40,12,12,40,64,40,64,14,14,14,40,40,64,64,64,40,64,12,12,12,40,40,64,16,16,16,64,64,12,12,12,12,12,12,64,64,64,64,40,64,64,40,12,12,12,12,12,12,64,40,40,14,64,40,64,12,1,40,1,40,1,14,16,16,16,1]]#,
#[ERROR,Ar,Ar,Ar,W,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,W,H,C,C,H,N,W,Ar,W,H,Ar,Ar,Cu,Ar,W,Ar,H,Ar,Ar,C,Ar,Ar,Ar,Ar,N,Ar,Ar,Ar,Ar,N,Ar,N,N,C,C,Ar,Al,Ar,Ar,Ar,Ar,Ar,H,Ar,Cu,C,Ar,N,N,N,Ar,N,C,C,C,Al,N,N,N,C,C,C,N,N,Ar,N,H,C,C,C,N,Ar,Ar,C,C,C,C,C,C,C,C,C,C,Ar,C,C,Ar,Cu,Ar,Cu,N,N,N,Ar,Ar,Cu,Cu,Cu,Ar,Cu,C,C,C,Ar,Ar,Cu,O,O,O,Cu,Cu,C,C,C,C,C,C,Cu,Cu,Cu,Cu,Ar,Cu,Cu,Ar,C,C,C,C,C,C,Cu,Ar,Ar,N,Cu,Ar,Cu,C,H,Ar,H,Ar,H,N,O,O,O,H]]


#[[-100,304.127,307.469,308.852,349.223,350.846,357.662,358.844,368.684,369.156,369.716,370.386,371.198,372.195,373.437,375.015,377.063,379.791,383.54,388.906,397.008,399.5,410.173,434.047,440.099,444.703,458.99,460.957,463.054,466.305,472.687,476.486,486.135,487.986,500.148,500.515,501.716,559.33,566.663,567.956,575.25,594.165,615.598,615.677,615.818,645.444,645.598,648.205,656.279,661.056,673.788,673.945,681.711,681.949,682.951,683.502,696.543,700.192,700.223,706.722,725.415,725.445,727.294,738.398,742.364,744.229,746.831,750.387,751.465,763.511,772.376,772.421,777.194,777.417,777.539,794.818],[100,27,27,27,27,27,40,40,2,2,2,2,2,2,2,2,2,2,2,2,2,28,2,2,40,28,40,40,28,40,40,40,2,40,28,28,40,27,28,28,28,28,16,16,16,16,16,28,2,28,45,45,45,45,45,45,40,16,16,40,16,16,40,40,28,28,28,40,40,40,40,40,16,16,16,40],['ERROR','Al II','Al II','Al II','Al IV','Al IV','Ar II','Ar II','H I','H I','H I','H I','H I','H I','H I','H I','H I','H I','H I','H I','H I','N II','H I','H I','Ar II','N II','Ar II','Ar II','N II','Al II','Ar II','Ar II','H I','Ar II','N II','N II','Ar II','Al II','N II','N II','N I','N II','O I','O I','O I','O I','O I','N II','H I','N II','Sc I','Sc I','Sc I','Sc I','Sc I','Sc I','Ar I','O I','O I','Ar I','O I','O I','Ar I','Ar I','N I','N I','N I','Ar I','Ar I','Ar I','Ar I','Ar I','O I','O I','O I','Ar I']]



def RGA_stuff(rga_data):
    
    ### this reformats the rga data
    mass = np.zeros(len(rga_data))
    rga = np.zeros(len(rga_data))
    if len(rga_data)<50:
        rga_data = np.zeros(100)
        print("data was short!")
    for i in range(len(rga_data)-1):
        mass[i] = i 
        rga[i] = rga_data[i]
        
    rga = np.transpose(rga)
    mass_spec = [mass,rga]

  
    
    
    b = mass_spec[1][0:81]  # here we prepare the incoming mass spec data

    #print(b)

    # these are the "test gases" in order.
    # Acetone, Air, Ar, CO, CO_2, H_2, H_2O, N_2, IPA, neon, methane, Fomblin oil, pump oil, O_2
    A = np.zeros((81,14))
    ### these are the cracking patterns for the various "test gases"
    #Acetone:
    A[15][0] = 0.2 ; A[43][0] = 1 ; A[58][0] = 0.33
    # Air: 78 % N2, 20 % O2, 1% H2O, 0.93% Ar, 0.04% CO2 
    A[12][1] = 0.000005025 ; A[14][1] = 0.1399; A[16][1] =  0.048; A[17][1] = 0.00242; A[18][1] = 0.0115
    A[19][1] = 0.0000069; A[20][1] = 0.002112; A[22][1] = 0.00001364; A[28][1] = 1; A[29][1] = 0.00699
    A[32][1] = 0.2205; A[34][1] = 0.00154; A[36][1] = 4.29e-5 ; A[38][1] = 7.15e-6 ; A[40][1] = 0.014306
    A[45][1] = 7.896e-6 ; A[46][1] = 2.871e-6 
    #Argon: 20 = 0.146, 36 = 0.003, 38 = 0.0005, 40 = 1
    A[20][2] = 0.146 ; A[36][2] = 0.003 ; A[38][2] = 0.0005 ; A[40][2] = 1 
    #CO: 12 = 0.047, 16 = 0.017, 28 = 1, 29 = 0.012
    A[12][3] = 0.047 ; A[16][3] = 0.017 ; A[28][3] = 1 ; A[29][3] = 0.012 ;
    # CO_2: 12 = 0.087, 16 = 0.1, 22 = 0.019, 28 = 0.10, 44 = 1, 45 = 0.011, 46  = 0.004
    A[12][4] = 0.087 ; A[16][4] = 0.1 ; A[22][4] = 0.019 ; A[28][4] = 0.1 ; A[44][4] = 1 
    A[45][4] = 0.011 ; A[46][4] = 0.004 
    #H_2: 1 = 0.021, 2 = 1
    A[1][5] = 1; A[2][5] = 1
    #H_2O: 16 = 0.01, 17 = 0.21, 18 = 1, 19 = 0.0006, 20 = 0.002
    A[16][6] = 0.01 ; A[17][6] = 0.21 ; A[18][6] = 1 ; A[19][6] = 0.0006 ; A[20][6] = 0.002
    #N_2: 14 = 0.14, 28 = 1, 29 = 0.007
    A[14][7] = 0.14 ; A[28][7] = 1 ; A[29][7] = 0.007 
    #IPA: 27 = 0.157, 43 = 0.166, 45 = 1
    A[27][8] = 0.157 ; A[43][8] = 0.166 ; A[45][8] = 1
    #neon: 20 = 1, 22 = 0.102
    A[20][9] = 1 ; A[22][9] = 0.102
    #Methane: 12 = 0.038, 13 = 0.107, 14 = 0.205, 15 = 0.85, 16 = 1, 17 = 0.011
    A[12][10] = 0.038 ; A[12][10] = 0.107 ; A[14][10] = 0.205 ; A[15][10] = 0.85 ; A[16][10] = 1 
    A[17][10] = 0.011 
    #Fomblin oil:
    A[16][11] = 0.16 ;  A[20][11] = 0.28 ; A[69][11] = 1 ;
    #Pump oil:
    A[43][12] = 0.733 ;  A[55][12] = 0.727 ; A[57][12] = 1 ;
    # O_2: 16 = 0.22, 32 = 1, 34 = 0.007 
    A[16][13] = 0.22 ; A[32][13] = 1; A[34][13] = 0.007
    
    #print(A)
    #print(A[20][9])
    Ainv = np.linalg.pinv(A)  # here we invert the matrix
    n = np.matmul(Ainv,b) # here we do the multiplication

    return(n)  # here we return the result

def DrawRGAFigure():
    
    plt.subplot(4,1,1)
    plt.plot(RGAData)
    plt.grid(b=True,which='both', axis = 'both')
    plt.yscale('log')
    plt.xlabel('Mass (amu)')
    plt.ylabel('Pressure (Torr)')
    plt.subplot(4,1,2)
    plt.grid(b=True,which='both', axis = 'both')
    plt.plot(n, 'rp')
    plt.yscale('log')
    plt.xticks(np.arange(14), labels, rotation = '60')
    plt.ylabel('Pressure (Torr)')
 
    
    plt.draw()




#### here is the expected format for the rga data

labels = ('Acetone', 'Air', 'Ar', 'CO', 'CO_2', 'H_2', 'H_2O', 'N_2', 'IPA', 'Neon', 'Methane', 'Fomblin Oil', 'Pump Oil', 'O_2')


while(True):
    ConnectionMade = False
    while(not ConnectionMade):
        
        try:
            MetrologyController = DeviceProxy("rareRF/MidLevel/MetrologyController")
            ConnectionMade = True
        except Exception as e:
            print(e)
            print(str(datetime.now()))


    while(ConnectionMade and MetrologyController.graphEnable):
        try:
            
            # Grab RGAData.
            RGAData = MetrologyController.RGASpectra
            
            if len(RGAData)>30:
                n = RGA_stuff(RGAData)
                RGAData = MetrologyController.RGASpectra
                n = RGA_stuff(RGAData)
                drawnow(DrawRGAFigure,stop_on_close=True)
                
            
            ### end of the program
        except Exception as e:
            print(e)
            print(str(datetime.now()))
            ConnectionMade = False
            
        plt.pause(MetrologyController.oesPollRate)
