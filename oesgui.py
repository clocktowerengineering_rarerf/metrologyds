# This code will relate the output from the RGA to possible componets solving Ax=b problem
#
# 2019-06-13 W. D. Lee -- originated
# 2019-06-27 W. D. Lee -- cleaned up the plots
# 2019-07-02 W. D. Lee -- fixed the matrix multiplication to be correct, imporved the commenting

import numpy as np
import matplotlib.pyplot as plt2
from datetime import datetime
from tango import DeviceProxy
from drawnow import drawnow, figure
import time
from scipy.signal import find_peaks

### this function builds the matrix, inverts it and does the multiplication, out putting a 
### vector of length equal to the number of "test gases" 

wavelengthtable = [[-100,356.052455,357.763636,358.946441,361.8547,365.769,365.829,365.908,365.969,366.045,366.136,366.231,366.326,366.445,366.569,366.712,366.877,367.05,367.237,367.486,367.7423,368.0418,368.3871,368.7881,369.2602,369.8209,370.4913,371.3034,372.3005,373.5431,375.1217,377.1704,379.8987,383.6485,386.9083,389.0166,392.009,392.18,397.1198,399.613,400.98839,407.31541,407.5508,410.2892,411.39756,413.288864,422.91329,427.873161,429.5814,433.241703,434.1692,434.928576,437.198114,437.26098,438.089709,440.22222,442.72436,443.14324,444.828,448.306791,454.632581,458.063257,459.118366,460.277,461.085816,463.184,464.438,464.872,465.155,465.920495,466.4361,472.819041,473.72302,476.619677,480.736348,484.916386,486.271,488.122627,491.110431,493.340225,496.64649,500.288,500.655,500.872,501.856194,504.651,505.355783,514.659,538.18269,559.4855,566.82,567.76,568.114,579.4722,580.2202,589.14,593.342,594.33,611.661581,648.384,656.46,657.987,658.47,658.9428,661.239,696.7352,706.9167,710.2081,711.0895,711.3436,711.5141,711.715,711.715,711.8952,712.1634,712.4159,713.4078,714.9012,723.331,723.841,727.494,732.8025,738.6014,740.19219,742.568,744.434,747.037,750.5935,751.6721,756.40961,758.111,758.1937,763.7208,765.4439,766.455,768.729,769.4612,772.5887,772.6333,772.8763,777.408,777.631,777.753,782.2729,782.7806,783.4784,783.9261,784.2427,785.0405,785.5023,786.304,789.2739,789.79771,790.47221,794.66221,795.0362,797.4224,799.8992,801.699,804.7546,806.081,806.4573,807.2635,808.0701,808.602,809.77528,810.5921,811.7542,821.86,823.7536,826.6794,827.98278,833.74351,839.471,841.0521,841.563,842.6963,844.027,844.106,844.857,844.868,844.908,846.959],
[3,40,40,40,184,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,184,1,12,12,1,14,184,40,184,1,40,40,64,40,184,40,1,40,40,12,40,40,40,40,14,40,40,40,40,14,40,14,14,12,12,40,27,40,40,40,40,40,1,40,64,12,40,14,14,14,40,14,12,12,12,27,14,14,14,12,12,12,14,14,40,14,1,12,12,12,14,40,40,12,12,12,12,12,12,12,12,12,12,40,12,12,40,64,40,64,14,14,14,40,40,64,64,64,40,64,12,12,12,40,40,64,16,16,16,64,64,12,12,12,12,12,12,64,64,64,64,40,64,64,40,12,12,12,12,12,12,64,40,40,14,64,40,64,12,1,40,1,40,1,14,16,16,16,1]]#,
#[ERROR,Ar,Ar,Ar,W,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,H,W,H,C,C,H,N,W,Ar,W,H,Ar,Ar,Cu,Ar,W,Ar,H,Ar,Ar,C,Ar,Ar,Ar,Ar,N,Ar,Ar,Ar,Ar,N,Ar,N,N,C,C,Ar,Al,Ar,Ar,Ar,Ar,Ar,H,Ar,Cu,C,Ar,N,N,N,Ar,N,C,C,C,Al,N,N,N,C,C,C,N,N,Ar,N,H,C,C,C,N,Ar,Ar,C,C,C,C,C,C,C,C,C,C,Ar,C,C,Ar,Cu,Ar,Cu,N,N,N,Ar,Ar,Cu,Cu,Cu,Ar,Cu,C,C,C,Ar,Ar,Cu,O,O,O,Cu,Cu,C,C,C,C,C,C,Cu,Cu,Cu,Cu,Ar,Cu,Cu,Ar,C,C,C,C,C,C,Cu,Ar,Ar,N,Cu,Ar,Cu,C,H,Ar,H,Ar,H,N,O,O,O,H]]


#[[-100,304.127,307.469,308.852,349.223,350.846,357.662,358.844,368.684,369.156,369.716,370.386,371.198,372.195,373.437,375.015,377.063,379.791,383.54,388.906,397.008,399.5,410.173,434.047,440.099,444.703,458.99,460.957,463.054,466.305,472.687,476.486,486.135,487.986,500.148,500.515,501.716,559.33,566.663,567.956,575.25,594.165,615.598,615.677,615.818,645.444,645.598,648.205,656.279,661.056,673.788,673.945,681.711,681.949,682.951,683.502,696.543,700.192,700.223,706.722,725.415,725.445,727.294,738.398,742.364,744.229,746.831,750.387,751.465,763.511,772.376,772.421,777.194,777.417,777.539,794.818],[100,27,27,27,27,27,40,40,2,2,2,2,2,2,2,2,2,2,2,2,2,28,2,2,40,28,40,40,28,40,40,40,2,40,28,28,40,27,28,28,28,28,16,16,16,16,16,28,2,28,45,45,45,45,45,45,40,16,16,40,16,16,40,40,28,28,28,40,40,40,40,40,16,16,16,40],['ERROR','Al II','Al II','Al II','Al IV','Al IV','Ar II','Ar II','H I','H I','H I','H I','H I','H I','H I','H I','H I','H I','H I','H I','H I','N II','H I','H I','Ar II','N II','Ar II','Ar II','N II','Al II','Ar II','Ar II','H I','Ar II','N II','N II','Ar II','Al II','N II','N II','N I','N II','O I','O I','O I','O I','O I','N II','H I','N II','Sc I','Sc I','Sc I','Sc I','Sc I','Sc I','Ar I','O I','O I','Ar I','O I','O I','Ar I','Ar I','N I','N I','N I','Ar I','Ar I','Ar I','Ar I','Ar I','O I','O I','O I','Ar I']]


def DrawOESFigure():
    
    plt2.subplot(2,1,1)
    plt2.title('Wavelengths (nm)')
    plt2.plot(OESWavelengths,OESIntensities)
    plt2.grid(b=True,which='both', axis = 'both')
    plt2.ylabel('Intensities')
    #plt2.yscale("log")
    plt2.ylim((MetrologyController.graphYMin,MetrologyController.graphYMax))
    plt2.subplot(2,1,2)
    #print np.array(OESAMU)
    plt2.plot(np.array(OESAMU))
    plt2.grid(b=True,which='both', axis = 'both')
    plt2.xlabel('# Peaks')
    plt2.xlabel('Mass (amu)')
    plt2.draw()


def PeakFinder():
    
    # from OESIntensities, find all peaks over intensity 50.
    
    peaksFound = find_peaks(MetrologyController.OESIntensity,height = MetrologyController.prominenceHeight, distance = MetrologyController.prominenceDistance)[0]
    wavelengthsFound = MetrologyController.OESWavelength[peaksFound]
    
    spectramatches = []
    spectraerror = []
    matchamu = []
    nomatchamu = []
    # ok, we now have peaks and wavelengths associated with that peak. find the closest match in the database
    for i in range(len(wavelengthsFound)):
        
        spectramatches.append(np.abs(np.asarray(wavelengthtable[0])-wavelengthsFound[i]).argmin())
        spectraerror.append(np.abs(wavelengthtable[0][spectramatches[i]]-wavelengthsFound[i]))
        if spectraerror[i] < 1:
            matchamu.append(wavelengthtable[1][spectramatches[i]])

            print("spectraerror(" + str(wavelengthsFound[i]) + "/" + str(peaksFound[i]) + "): " + str(spectraerror[-1]) + " ; AMU found: " + str(matchamu[-1]) + " / " + str(wavelengthtable[0][spectramatches[-1]]))
        
        else:
            nomatchamu.append(wavelengthsFound[i])
            print("missing peak: " + str(wavelengthsFound[i]) + "/" + str(peaksFound[i]))
            
    
            
    # great, now we have an array of amu matches. sweep all AMU and count if it's that AMU.
    print("# peaks found: " + str(len(matchamu)))
    
    OESAMU = [0]*80
    for i in range(80):
        
        OESAMU[i] = matchamu.count(i)
        #print OESAMU[i]
    
    return OESAMU #, matchamu, nomatchamu, wavelengthsFound
    # ok, now we have a count of AMU matches by AMU the OES sees! plot OESAMU.




while(True):
    ConnectionMade = False
    while(not ConnectionMade):
        
        try:
            MetrologyController = DeviceProxy("rareRF/MidLevel/MetrologyController")
            ConnectionMade = True
        except Exception as e:
            print(e)
            print(str(datetime.now()))


    while(ConnectionMade and MetrologyController.graphEnable):
        try:
            
            # Grab OESData.
            OESIntensities = MetrologyController.OESIntensity 
            OESWavelengths = MetrologyController.OESWavelength
            OESAMU = PeakFinder()
            if len(OESIntensities)>1000:
                drawnow(DrawOESFigure,stop_on_close=True)             
                
            
            ### end of the program
            
        except Exception as e:
            print(e)
            print(str(datetime.now()))
            ConnectionMade = False
            
        plt2.pause(MetrologyController.oesPollRate)
